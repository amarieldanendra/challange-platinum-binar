import React, { useEffect, useState, PureComponent } from "react";
import { useSelector, useDispatch } from "react-redux";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from "recharts";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import axios from "axios";

const DataDashboard = (props) => {
  const dataA = [
    {
      name: "Page A",
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: "Page B",
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: "Page C",
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: "Page D",
      uv: 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: "Page E",
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: "Page F",
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: "Page G",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page H",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page I",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page J",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page K",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page L",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page M",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page N",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page O",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page P",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page Q",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page R",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page S",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page T",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page U",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page V",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page W",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page X",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page Y",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page Z",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page AA",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page BB",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page CC",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: "Page DD",
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
  ];

  const columns = [
    {
      dataField: "no",
      text: "No",
    },
    {
      dataField: "email",
      text: "User Email",
      sort: true,
    },
    {
      dataField: "startRent",
      text: "Start Rent",
      sort: true,
    },
    {
      dataField: "finishRent",
      text: "Finish Rent",
      sort: true,
    },
    {
      dataField: "price",
      text: "Price",
      sort: true,
    },
    {
      dataField: "category",
      text: "Category",
      sort: true,
    },
  ];

  console.log(props.props);

  const [data, setData] = useState([]);
  const [date, setDate] = useState(new Date());
  const [dataList, setDataList] = useState([]);
  useEffect(() => {
    const token = localStorage.getItem("token");
    axios
      .get("https://bootcamp-rent-cars.herokuapp.com/admin/order/reports?from=2022-10-01&until=2022-10-31", {
        headers: {
          access_token: token,
        },
      })
      .then((rest) => {
        let tempData = [];
        for (let row of rest.data) {
          tempData.push({
            name: new Date(row.day).getDate(),
            order: row.orderCount,
          });
        }
        setData(tempData);
      });
  }, []);
  useEffect(() => {
    const token = localStorage.getItem("token");
    axios
      .get("https://bootcamp-rent-cars.herokuapp.com/admin/v2/order", {
        headers: {
          access_token: token,
        },
      })
      .then((res) => {
        let tempData = [];
        let count = 1;
        for (let row of res.data.orders) {
          tempData.push({
            no: count,
            email: row.User.email,
            startRent: row.start_rent_at.split("T")[0],
            finishRent: row.finish_rent_at.split("T")[0],
            price: row.total_price,
            category: "",
          });
          count++;
        }
        setDataList(tempData);
      });
  });

  const onSubmit = () => {
    const token = localStorage.getItem("token");
    axios
      .get(`https://bootcamp-rent-cars.herokuapp.com/admin/order/reports?from=${date.toISOString().split("T")[0]}&until=${new Date(date.getFullYear(), date.getMonth() + 1, 0).toISOString().split("T")[0]}`, {
        headers: {
          access_token: token,
        },
      })
      .then((rest) => {
        let tempData = [];
        for (let row of rest.data) {
          tempData.push({
            name: new Date(row.day).getDate(),
            order: row.orderCount,
          });
        }
        setData(tempData);
      });
  };

  return (
    <div className="container-fluid d-flex ps-0" style={{ backgroundColor: "#E5E5E5", height: "100%" }}>
      <div className="menu flex-1">
        <h5 className="menu-list" style={{ color: "grey" }}>
          DASHBOARD
        </h5>
        <h5 className="menu-item">Dashboard</h5>
      </div>
      <div className="content">
        <div className="row">
          <div className="col pb-1">
            <div className="breadcrumbs d-flex mt-4 pt-2">
              <h5>Dashboard</h5>
              <span>&nbsp; > &nbsp;</span>
              <p className="mb-2">Dashboard</p>
            </div>
            <div className="row mb-4">
              <div className="title-page d-flex mb-2">
                <div className="rectangle-mini me-2"></div>
                <h5 className="mb-2">Rented Car Data Visualization</h5>
              </div>
            </div>
          </div>
        </div>

        <div className="data-chart" style={{ width: "100%" }}>
          <p className="mb-1">Month</p>
          <div className="d-flex">
            <input onChange={(e) => setDate(e.currentTarget.valueAsDate)} type="month" name="kategori" style={{ width: "150px" }} className="border border-2 form-control" />
            <button onClick={onSubmit} className="btn btn-primary">
              Go
            </button>
          </div>
          {/* <ResponsiveContainer width="100%"> */}
          <BarChart width={1050} height={450} data={data}>
            <XAxis dataKey="name" scale="point" padding={{ left: 10, right: 10, bottom: 10 }}>
              <Label value="Date" offset={0} position="insideBottom" />
            </XAxis>
            <YAxis label={{ value: "Amount of Car Rented", angle: -90, position: "insideLeft" }} />
            <Tooltip />
            <CartesianGrid strokeDasharray="3 3" />
            <Bar dataKey="order" fill="#586B90" />
          </BarChart>
          {/* </ResponsiveContainer> */}
        </div>

        <div className="data-tabel mt-5">
          <h2>Dashboard</h2>
          <div className="title-page d-flex mb-2">
            <div className="rectangle-mini me-2"></div>
            <h5 className="mb-2">Data List</h5>
          </div>
          <BootstrapTable keyField="id" data={dataList} columns={columns} pagination={paginationFactory()} headerClasses="header-class" />
        </div>
      </div>
    </div>
  );
};

export default DataDashboard;

import React from "react";
import FetchProduct from "../Component/fetchProduct";
import NavigationBar from "../Component/NavigationBar";
import NavigationHeader from "../Component/NavigationHeader";
import { useLocation } from "react-router-dom";

const Cars = () => {
  const location = useLocation();
  const data = location.state;
  return (
    <div className="d-flex">
      <NavigationBar />
      <div className="flex-10">
        <NavigationHeader />
        <FetchProduct props={data} />
      </div>
    </div>
  );
};
export default Cars;
